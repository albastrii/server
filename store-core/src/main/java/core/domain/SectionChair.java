package core.domain;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@Builder
public class SectionChair extends BaseEntity<Integer> {
    private String firstName;
    private String lastName;
    private int age;
}
