package core.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class Evaluation extends BaseEntity<Integer> {
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "reviewerId")
    private Reviewer reviewer;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "paperId")
    private Paper paper;

    private String result;
}
