package core.domain;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@Builder
public class Paper extends BaseEntity<Integer> {

    @OneToOne(cascade = CascadeType.ALL)
    private Proposal proposal;

//    private int proposalID;
    private String title;
    private String document;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "paper", cascade = CascadeType.ALL)
    List<Evaluation> evaluations;
}
