package core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@Builder
@Inheritance(strategy = InheritanceType.JOINED)
public class Participant extends BaseEntity<Integer> {
    @JsonIgnore
    @OneToMany(mappedBy = "participant", fetch = FetchType.EAGER)
    List<Fee> fees;

    private String firstName;
    private String lastName;
    private int age;

    @ManyToOne
    private Login login;
}
