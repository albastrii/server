package core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class Fee extends BaseEntity<Integer>{
    @ManyToOne
    @JoinColumn(name = "conferenceId")
    @JsonIgnore
    private Conference conference;

    @ManyToOne
    @JoinColumn(name = "participantId")
    @JsonIgnore
    private Participant participant;

    private Date date;
}
