@startuml

title __DOMAIN's Class Diagram__\n

  package core.domain {
    class Author {
        - position : String
        + toString()
        + getPosition()
        + getProposal()
        + setPosition()
        + setProposal()
        + Author()
        + equals()
        # canEqual()
        + hashCode()
        + Author()
    }
  }
  

  package core.domain {
    class BaseEntity {
        + toString()
        + getId()
        + setId()
        + equals()
        # canEqual()
        + hashCode()
        + BaseEntity()
    }
  }
  

  package core.domain {
    class Bidding {
        - result : String
        {static} + builder()
        + toString()
        + getReviewer()
        + getProposal()
        + getResult()
        + setReviewer()
        + setProposal()
        + setResult()
        + equals()
        # canEqual()
        + hashCode()
        + Bidding()
        + Bidding()
    }
  }
  

  package core.domain {
    class BiddingBuilder {
        - result : String
        ~ BiddingBuilder()
        + reviewer()
        + proposal()
        + result()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Chair {
        - position : String
        + toString()
        + getPosition()
        + setPosition()
        + Chair()
        + equals()
        # canEqual()
        + hashCode()
        + Chair()
    }
  }
  

  package core.domain {
    class Conference {
        - name : String
        - topic : String
        - description : String
        - location : String
        - startDate : Date
        - endDate : Date
        - amount : int
        - paperDeadline : Date
        - abstractDeadline : Date
        ~ fees : List<Fee>
        - sections : List<Section>
        - proposals : List<Proposal>
        - pcMembers : List<PCMember>
        {static} + builder()
        + toString()
        + getName()
        + getTopic()
        + getDescription()
        + getLocation()
        + getStartDate()
        + getEndDate()
        + getAmount()
        + getPaperDeadline()
        + getAbstractDeadline()
        + getFees()
        + getSections()
        + getProposals()
        + getPcMembers()
        + setName()
        + setTopic()
        + setDescription()
        + setLocation()
        + setStartDate()
        + setEndDate()
        + setAmount()
        + setPaperDeadline()
        + setAbstractDeadline()
        + setFees()
        + setSections()
        + setProposals()
        + setPcMembers()
        + Conference()
        + equals()
        # canEqual()
        + hashCode()
        + Conference()
    }
  }
  

  package core.domain {
    class ConferenceBuilder {
        - name : String
        - topic : String
        - description : String
        - location : String
        - startDate : Date
        - endDate : Date
        - amount : int
        - paperDeadline : Date
        - abstractDeadline : Date
        - fees : List<Fee>
        - sections : List<Section>
        - proposals : List<Proposal>
        - pcMembers : List<PCMember>
        ~ ConferenceBuilder()
        + name()
        + topic()
        + description()
        + location()
        + startDate()
        + endDate()
        + amount()
        + paperDeadline()
        + abstractDeadline()
        + fees()
        + sections()
        + proposals()
        + pcMembers()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Evaluation {
        - result : String
        {static} + builder()
        + toString()
        + getReviewer()
        + getPaper()
        + getResult()
        + setReviewer()
        + setPaper()
        + setResult()
        + equals()
        # canEqual()
        + hashCode()
        + Evaluation()
        + Evaluation()
    }
  }
  

  package core.domain {
    class EvaluationBuilder {
        - result : String
        ~ EvaluationBuilder()
        + reviewer()
        + paper()
        + result()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Fee {
        - date : Date
        {static} + builder()
        + toString()
        + getConference()
        + getParticipant()
        + getDate()
        + setConference()
        + setParticipant()
        + setDate()
        + equals()
        # canEqual()
        + hashCode()
        + Fee()
        + Fee()
    }
  }
  

  package core.domain {
    class FeeBuilder {
        - date : Date
        ~ FeeBuilder()
        + conference()
        + participant()
        + date()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Listener {
        - sections : List<Section>
        + toString()
        + getSections()
        + setSections()
        + Listener()
        + equals()
        # canEqual()
        + hashCode()
        + Listener()
    }
  }
  

  package core.domain {
    class Login {
        - username : String
        - password : String
        - role : String
        {static} + builder()
        + toString()
        + getUsername()
        + getPassword()
        + getRole()
        + setUsername()
        + setPassword()
        + setRole()
        + Login()
        + equals()
        # canEqual()
        + hashCode()
        + Login()
    }
  }
  

  package core.domain {
    class LoginBuilder {
        - username : String
        - password : String
        - role : String
        ~ LoginBuilder()
        + username()
        + password()
        + role()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class PCMember {
        - conferences : List<Conference>
        - name : String
        - affiliation : String
        - email : String
        - website : String
        {static} + builder()
        + toString()
        + getConferences()
        + getName()
        + getAffiliation()
        + getEmail()
        + getWebsite()
        + getLogin()
        + setConferences()
        + setName()
        + setAffiliation()
        + setEmail()
        + setWebsite()
        + setLogin()
        + PCMember()
        + equals()
        # canEqual()
        + hashCode()
        + PCMember()
    }
  }
  

  package core.domain {
    class PCMemberBuilder {
        - conferences : List<Conference>
        - name : String
        - affiliation : String
        - email : String
        - website : String
        ~ PCMemberBuilder()
        + conferences()
        + name()
        + affiliation()
        + email()
        + website()
        + login()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Paper {
        - title : String
        - document : String
        ~ evaluations : List<Evaluation>
        {static} + builder()
        + toString()
        + getProposal()
        + getTitle()
        + getDocument()
        + getEvaluations()
        + setProposal()
        + setTitle()
        + setDocument()
        + setEvaluations()
        + Paper()
        + equals()
        # canEqual()
        + hashCode()
        + Paper()
    }
  }
  

  package core.domain {
    class PaperBuilder {
        - title : String
        - document : String
        - evaluations : List<Evaluation>
        ~ PaperBuilder()
        + proposal()
        + title()
        + document()
        + evaluations()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Participant {
        ~ fees : List<Fee>
        - firstName : String
        - lastName : String
        - age : int
        {static} + builder()
        + toString()
        + getFees()
        + getFirstName()
        + getLastName()
        + getAge()
        + getLogin()
        + setFees()
        + setFirstName()
        + setLastName()
        + setAge()
        + setLogin()
        + Participant()
        + equals()
        # canEqual()
        + hashCode()
        + Participant()
    }
  }
  

  package core.domain {
    class ParticipantBuilder {
        - fees : List<Fee>
        - firstName : String
        - lastName : String
        - age : int
        ~ ParticipantBuilder()
        + fees()
        + firstName()
        + lastName()
        + age()
        + login()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Proposal {
        - title : String
        - description : String
        - abstractProposal : String
        ~ biddings : List<Bidding>
        {static} + builder()
        + toString()
        + getAuthor()
        + getTitle()
        + getDescription()
        + getAbstractProposal()
        + getPaper()
        + getBiddings()
        + setAuthor()
        + setTitle()
        + setDescription()
        + setAbstractProposal()
        + setPaper()
        + setBiddings()
        + Proposal()
        + equals()
        # canEqual()
        + hashCode()
        + Proposal()
    }
  }
  

  package core.domain {
    class ProposalBuilder {
        - title : String
        - description : String
        - abstractProposal : String
        - biddings : List<Bidding>
        ~ ProposalBuilder()
        + author()
        + title()
        + description()
        + abstractProposal()
        + paper()
        + biddings()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Reviewer {
        - domainOfInterest : String
        ~ biddings : List<Bidding>
        ~ evaluations : List<Evaluation>
        + toString()
        + getDomainOfInterest()
        + getBiddings()
        + getEvaluations()
        + setDomainOfInterest()
        + setBiddings()
        + setEvaluations()
        + Reviewer()
        + equals()
        # canEqual()
        + hashCode()
        + Reviewer()
    }
  }
  

  package core.domain {
    class Section {
        - name : String
        {static} + builder()
        + toString()
        + getName()
        + getSectionChair()
        + setName()
        + setSectionChair()
        + Section()
        + equals()
        # canEqual()
        + hashCode()
        + Section()
    }
  }
  

  package core.domain {
    class SectionBuilder {
        - name : String
        ~ SectionBuilder()
        + name()
        + sectionChair()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class SectionChair {
        - firstName : String
        - lastName : String
        - age : int
        {static} + builder()
        + toString()
        + getFirstName()
        + getLastName()
        + getAge()
        + setFirstName()
        + setLastName()
        + setAge()
        + SectionChair()
        + equals()
        # canEqual()
        + hashCode()
        + SectionChair()
    }
  }
  

  package core.domain {
    class SectionChairBuilder {
        - firstName : String
        - lastName : String
        - age : int
        ~ SectionChairBuilder()
        + firstName()
        + lastName()
        + age()
        + build()
        + toString()
    }
  }
  

  package core.domain {
    class Speaker {
        - presentation : String
        + toString()
        + getSection()
        + getPaper()
        + getPresentation()
        + setSection()
        + setPaper()
        + setPresentation()
        + Speaker()
        + equals()
        # canEqual()
        + hashCode()
        + Speaker()
    }
  }
  

  Author -up-|> Participant
  Author o-- Proposal : proposal
  ParticipantBuilder o-- Login : login
  BaseEntity o-- ID : id
  Bidding -up-|> BaseEntity
  Bidding o-- Reviewer : reviewer
  Bidding o-- Proposal : proposal
  Bidding +-down- BiddingBuilder
  BiddingBuilder o-- Reviewer : reviewer
  BiddingBuilder o-- Proposal : proposal
  Chair -up-|> PCMember
  PCMemberBuilder o-- Login : login
  Conference -up-|> BaseEntity
  Conference +-down- ConferenceBuilder
  Evaluation -up-|> BaseEntity
  Evaluation o-- Reviewer : reviewer
  Evaluation o-- Paper : paper
  Evaluation +-down- EvaluationBuilder
  EvaluationBuilder o-- Reviewer : reviewer
  EvaluationBuilder o-- Paper : paper
  Fee -up-|> BaseEntity
  Fee o-- Conference : conference
  Fee o-- Participant : participant
  Fee +-down- FeeBuilder
  FeeBuilder o-- Conference : conference
  FeeBuilder o-- Participant : participant
  Listener -up-|> Participant
  ParticipantBuilder o-- Login : login
  Login +-down- LoginBuilder
  PCMember -up-|> BaseEntity
  PCMember o-- Login : login
  PCMember +-down- PCMemberBuilder
  PCMemberBuilder o-- Login : login
  Paper -up-|> BaseEntity
  Paper o-- Proposal : proposal
  Paper +-down- PaperBuilder
  PaperBuilder o-- Proposal : proposal
  Participant -up-|> BaseEntity
  Participant o-- Login : login
  Participant +-down- ParticipantBuilder
  ParticipantBuilder o-- Login : login
  Proposal -up-|> BaseEntity
  Proposal o-- Author : author
  Proposal o-- Paper : paper
  Proposal +-down- ProposalBuilder
  ProposalBuilder o-- Author : author
  ProposalBuilder o-- Paper : paper
  Reviewer -up-|> PCMember
  PCMemberBuilder o-- Login : login
  Section -up-|> BaseEntity
  Section o-- SectionChair : sectionChair
  Section +-down- SectionBuilder
  SectionBuilder o-- SectionChair : sectionChair
  SectionChair -up-|> BaseEntity
  SectionChair +-down- SectionChairBuilder
  Speaker -up-|> Author
  Speaker o-- Section : section
  Speaker o-- Paper : paper
  ParticipantBuilder o-- Login : login


right footer


PlantUML diagram generated by SketchIt! (https://bitbucket.org/pmesmeur/sketch.it)
For more information about this tool, please contact philippe.mesmeur@gmail.com
endfooter

@enduml
