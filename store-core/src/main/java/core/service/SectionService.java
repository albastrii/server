package core.service;

import core.domain.Conference;
import core.domain.Section;
import core.domain.SectionChair;
import core.repository.SectionChairRepoI;
import core.repository.SectionRepoI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SectionService {
    @Autowired
    private SectionRepoI repo;

    public boolean addSection(Section section) {
        Optional<Section> sect = repo.findById(section.getId());
        if (sect.isPresent())
            return false;
        repo.save(section);
        return true;
    }

    public Optional<Section> findOneSection(Integer id) {
        return repo.findById(id);
    }
}
