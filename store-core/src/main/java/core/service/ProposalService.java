package core.service;

import core.domain.*;
import core.repository.BiddingRepoI;
import core.repository.PaperRepoI;
import core.repository.ProposalRepoI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProposalService {

    @Autowired
    private ProposalRepoI repo;

    @Autowired
    private BiddingRepoI biddingRepo;

    public boolean addProposal(Proposal proposal) {
        Optional<Proposal> pro = repo.findById(proposal.getId());
        if (pro.isPresent())
            return false;
        repo.save(proposal);
        return true;
    }

    public Optional<Proposal> findOneProposal(Integer id) {
        return repo.findById(id);
    }

//    public boolean addBidding(Bidding bidding) {
//        Optional<Bidding> pro = biddingRepo.findById(bidding.getId());
//        if (pro.isPresent())
//            return false;
//        biddingRepo.save(bidding);
//        return true;
//    }

    @Transactional
    public boolean addBidding(Bidding bidding) {
        try {
            Proposal p = repo.findById(bidding.getProposal().getId()).get();
            List<Bidding> biddings = p.getBiddings();
            Bidding nb = Bidding.builder()
                    .reviewer(bidding.getReviewer())
                    .proposal(p)
                    .result(bidding.getResult())
                    .build();
            biddings.add(nb);
            p.setBiddings(biddings);
            repo.save(p);
            System.out.println(1);
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    @Transactional
    public Optional<Proposal> findProposalByAuthorId(Integer authorId) {
        return repo.findAll().stream().filter(p -> p.getAuthor().getId() == authorId).findAny();
    }
}
