package core.service;

import core.domain.Conference;
import core.domain.Proposal;
import core.domain.Section;
import core.repository.ConferenceRepoI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ConferenceService {

    @Autowired
    private ConferenceRepoI repo;

    public boolean addConference(Conference conference) {
        Optional<Conference> conf = repo.findById(conference.getId());
        if (conf.isPresent())
            return false;
        repo.save(conference);
        return true;
    }

    public List<Conference> getConferences() {
        return new ArrayList<>(this.repo.findAll());
    }

    public Optional<Conference> findOneConference(Integer id) {
        return repo.findById(id);
    }

    public boolean addSectionToConference(Integer conferenceID, Section section) {
        Conference conference = this.repo.findById(conferenceID).get();
        List<Section> sections = conference.getSections();
        System.out.println(sections);
        sections.add(section);
        this.repo.save(conference);
        return true;
    }

    public boolean updateConferenceDeadlines(Conference conference) {
        Conference c = this.repo.findById(conference.getId()).get();
        //if the new dates are earlier than the old dates, return false
        if(conference.getAbstractDeadline().compareTo(c.getAbstractDeadline())<0 || conference.getPaperDeadline().compareTo(c.getPaperDeadline())<0)
            return false;
        c.setAbstractDeadline(conference.getAbstractDeadline());
        c.setPaperDeadline(conference.getPaperDeadline());
        this.repo.save(c);
        return true;
    }

    public List<Proposal> findProposalsByConference(Integer conferenceId) {
        Conference c = this.repo.findById(conferenceId).get();
        return c.getProposals();
    }
}
