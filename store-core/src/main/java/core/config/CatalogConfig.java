package core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"core.repository", "core.service", "core.domain"})
public class CatalogConfig {
}
