package core.repository;

import core.domain.Paper;

public interface PaperRepoI extends RepoI<Integer, Paper> {

}
