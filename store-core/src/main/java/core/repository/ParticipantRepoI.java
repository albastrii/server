package core.repository;

import core.domain.Participant;

public interface ParticipantRepoI extends RepoI<Integer, Participant> {

}
