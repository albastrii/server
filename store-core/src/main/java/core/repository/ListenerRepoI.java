package core.repository;

import core.domain.Listener;

public interface ListenerRepoI extends RepoI<Integer, Listener> {

}
