package core.repository;

import core.domain.Bidding;

public interface BiddingRepoI extends RepoI<Integer, Bidding> {

}
