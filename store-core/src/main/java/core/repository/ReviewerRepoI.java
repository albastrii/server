package core.repository;

import core.domain.Reviewer;

public interface ReviewerRepoI extends RepoI<Integer, Reviewer> {

}
