package core.repository;

import core.domain.Conference;

public interface ConferenceRepoI extends RepoI<Integer, Conference> {

}