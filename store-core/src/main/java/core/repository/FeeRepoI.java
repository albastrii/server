package core.repository;

import core.domain.Fee;

public interface FeeRepoI extends RepoI<Integer, Fee> {

}
