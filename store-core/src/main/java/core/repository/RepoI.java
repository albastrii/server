package core.repository;
import core.domain.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import java.io.Serializable;

@NoRepositoryBean
public interface RepoI<ID extends Serializable, T> extends JpaRepository<T, ID> {
}
