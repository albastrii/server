package core.repository;

import core.domain.Author;

public interface AuthorRepoI extends RepoI<Integer, Author> {

}
