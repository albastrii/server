package core.repository;

import core.domain.Chair;

public interface ChairRepoI extends RepoI<Integer, Chair> {

}
