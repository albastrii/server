package core.repository;

import core.domain.Section;

public interface SectionRepoI extends RepoI<Integer, Section> {

}
