package core.repository;

import core.domain.Speaker;

public interface SpeakerRepoI extends RepoI<Integer, Speaker> {

}
