package core.repository;

import core.domain.Proposal;

public interface ProposalRepoI extends RepoI<Integer, Proposal> {

}
