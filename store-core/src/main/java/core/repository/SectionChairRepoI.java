package core.repository;

import core.domain.SectionChair;

public interface SectionChairRepoI extends RepoI<Integer, SectionChair> {

}
