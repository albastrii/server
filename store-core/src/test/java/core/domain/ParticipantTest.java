package core.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ParticipantTest {

    @Test
    public void getFees() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        assertEquals(p.getFees(),f);
    }

    @Test
    public void getFirstName() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        assertEquals(p.getFirstName(),"fn");
    }

    @Test
    public void getLastName() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        assertEquals(p.getLastName(),"ln");
    }

    @Test
    public void getAge() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        assertEquals(p.getAge(),15);
    }

    @Test
    public void getLogin() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        assertEquals(p.getLogin(),l);
    }

    @Test
    public void setFees() {
        List<Fee> f=new ArrayList<>();
        List<Fee> f2=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        p.setFees(f2);
        assertEquals(p.getFees(),f2);
    }

    @Test
    public void setFirstName() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        p.setFirstName("fn2");
        assertEquals(p.getFirstName(),"fn2");
    }

    @Test
    public void setLastName() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        p.setLastName("ln2");
        assertEquals(p.getLastName(),"ln2");
    }

    @Test
    public void setAge() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        p.setAge(16);
        assertEquals(p.getAge(),16);
    }

    @Test
    public void setLogin() {
        List<Fee> f=new ArrayList<>();
        Login l = new Login();
        Login l2 = new Login();
        Participant p = new Participant(f,"fn","ln",15,l);
        p.setLogin(l2);
        assertEquals(p.getLogin(),l2);
    }
}
