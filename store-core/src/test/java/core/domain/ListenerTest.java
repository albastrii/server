package core.domain;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ListenerTest {

    @Test
    public void getSections() {
        Section s = new Section("daniel",null);
        ArrayList<Section> l = new ArrayList<>();
        l.add(s);
        Listener list = new Listener(l);
        assertEquals(list.getSections().get(0).getName(),s.getName());
    }

    @Test
    public void setSections() {
        Section s = new Section("daniel",null);
        Section s2 = new Section("daniel2",null);
        ArrayList<Section> l = new ArrayList<>();
        l.add(s);


        ArrayList<Section> l2 = new ArrayList<>();
        l2.add(s2);
        Listener list = new Listener(l2);
        assertEquals(list.getSections().get(0).getName(),s2.getName());
    }
}
