package core.domain;

import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

public class FeeTest {

    @Test
    public void getConference() {
        Conference c1 = new Conference();
        Participant p1 = new Participant();
        Fee f = new Fee(c1, p1, Date.valueOf("2019-01-01"));
        assertEquals(f.getConference(), c1);
    }

    @Test
    public void getParticipant() {
        Conference c1 = new Conference();
        Participant p1 = new Participant();
        Fee f = new Fee(c1, p1, Date.valueOf("2019-01-01"));
        assertEquals(f.getParticipant(), p1);
    }

    @Test
    public void getDate() {
        Conference c1 = new Conference();
        Participant p1 = new Participant();
        Fee f = new Fee(c1, p1, Date.valueOf("2019-01-01"));
        assertEquals(f.getDate(), Date.valueOf("2019-01-01"));
    }

    @Test
    public void setConference() {
        Conference c1 = new Conference();
        Conference c2 = new Conference();
        Participant p1 = new Participant();
        Fee f = new Fee(c1, p1, Date.valueOf("2019-01-01"));
        f.setConference(c2);
        assertEquals(f.getConference(), c2);
    }

    @Test
    public void setParticipant() {
        Conference c1 = new Conference();
        Participant p1 = new Participant();
        Participant p2 = new Participant();
        Fee f = new Fee(c1, p1, Date.valueOf("2019-01-01"));
        f.setParticipant(p2);
        assertEquals(f.getParticipant(), p2);
    }

    @Test
    public void setDate() {
        Conference c1 = new Conference();
        Participant p1 = new Participant();
        Fee f = new Fee(c1, p1, Date.valueOf("2019-01-01"));
        f.setDate(Date.valueOf("2019-02-02"));
        assertEquals(f.getDate(), Date.valueOf("2019-02-02"));
    }
}
