package core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class ChairTest {

    @Test
    public void getPosition() {

        Chair c = new Chair("abc");
        assertEquals(c.getPosition(),"abc");
    }

    @Test
    public void setPosition() {

        Chair c = new Chair("abc");
        c.setPosition("cba");
        assertEquals(c.getPosition(),"cba");
    }
}
