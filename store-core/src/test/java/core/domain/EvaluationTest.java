package core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class EvaluationTest {

    @Test
    public void getReviewer() {
        Reviewer r = new Reviewer();
        Paper p = new Paper();
        Evaluation e = new Evaluation(r,p,"abc");
        assertEquals(e.getReviewer(),r);
    }

    @Test
    public void getPaper() {
        Reviewer r = new Reviewer();
        Paper p = new Paper();
        Evaluation e = new Evaluation(r,p,"abc");
        assertEquals(e.getPaper(),p);
    }

    @Test
    public void getResult() {
        Reviewer r = new Reviewer();
        Paper p = new Paper();
        Evaluation e = new Evaluation(r,p,"abc");
        assertEquals(e.getResult(),"abc");
    }

    @Test
    public void setReviewer() {
        Reviewer r = new Reviewer();
        Reviewer r2 = new Reviewer("abc",null,null);
        Paper p = new Paper();
        Evaluation e = new Evaluation(r,p,"abc");
        e.setReviewer(r2);
        assertEquals(e.getReviewer(),r2);
    }

    @Test
    public void setPaper() {
        Reviewer r = new Reviewer();
        Paper p = new Paper();
        Paper p2 =  new Paper(null,"t1",null,null);
        Evaluation e = new Evaluation(r,p,"abc");
        e.setPaper(p2);
        assertEquals(e.getPaper(),p2);
    }

    @Test
    public void setResult() {
        Reviewer r = new Reviewer();
        Paper p = new Paper();
        Evaluation e = new Evaluation(r,p,"abc");
        e.setResult("cab");
        assertEquals(e.getResult(),"cab");
    }
}
