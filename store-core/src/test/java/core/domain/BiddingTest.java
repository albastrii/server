package core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class BiddingTest {

    @Test
    public void getReviewer() {
        Reviewer r = new Reviewer();
        Proposal p = new Proposal();
        Bidding b = new Bidding(r,p,"result");

        assertEquals(b.getReviewer(),r);

    }

    @Test
    public void getProposal() {
        Reviewer r = new Reviewer();
        Proposal p = new Proposal();
        Bidding b = new Bidding(r,p,"result");

        assertEquals(b.getProposal(),p);
    }

    @Test
    public void getResult() {
        Reviewer r = new Reviewer();
        Proposal p = new Proposal();
        Bidding b = new Bidding(r,p,"result");

        assertEquals(b.getResult(),"result");
    }

    @Test
    public void setReviewer() {
        Reviewer r = new Reviewer();
        Proposal p = new Proposal();
        Bidding b = new Bidding(r,p,"result");
        Reviewer r2 = new Reviewer("doi",null,null);
        b.setReviewer(r2);
        assertEquals(b.getReviewer(),r2);
    }

    @Test
    public void setProposal() {
        Reviewer r = new Reviewer();
        Proposal p = new Proposal();
        Bidding b = new Bidding(r,p,"result");
        Proposal p2 = new Proposal(null,"t1",null,null,null,null);
        b.setProposal(p2);
        assertEquals(b.getProposal(),p2);
    }

    @Test
    public void setResult() {
        Reviewer r = new Reviewer();
        Proposal p = new Proposal();
        Bidding b = new Bidding(r,p,"result");
        b.setResult("newresult");
        assertEquals(b.getResult(),"newresult");
    }
}
