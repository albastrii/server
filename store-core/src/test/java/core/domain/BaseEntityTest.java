package core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BaseEntityTest {

    @Test
    public void toString1() {
        BaseEntity b = new BaseEntity<Integer>();
        assertEquals(b.toString(), "ID: " + b.getId() + " - ");
    }

    @Test
    public void getId() {
        BaseEntity b = new BaseEntity<Integer>();
        assert (b.getId() == null);
    }

    @Test
    public void setId() {
        BaseEntity b = new BaseEntity<Integer>();
        b.setId(4);
        assertEquals(b.getId(), 4);
    }
}
