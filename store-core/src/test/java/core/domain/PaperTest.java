package core.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PaperTest {

    @Test
    public void getProposal() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        assertEquals(p.getProposal(),p1);
    }

    @Test
    public void getTitle() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        assertEquals(p.getTitle(),"t1");
    }

    @Test
    public void getDocument() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        assertEquals(p.getDocument(),"d1");
    }

    @Test
    public void getEvaluations() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        assertEquals(p.getEvaluations(),e1);
    }

    @Test
    public void setProposal() {
        Proposal p1 = new Proposal();
        Proposal p2 = new Proposal(null,"t1",null,null,null,null);
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        p.setProposal(p2);
        assertEquals(p.getProposal(),p2);
    }

    @Test
    public void setTitle() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        p.setTitle("t2");
        assertEquals(p.getTitle(),"t2");
    }

    @Test
    public void setDocument() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);
        Paper p = new Paper(p1, "t1", "d1", e1);
        p.setDocument("d2");
        assertEquals(p.getDocument(),"d2");
    }

    @Test
    public void setEvaluations() {
        Proposal p1 = new Proposal();
        Evaluation e = new Evaluation();
        List<Evaluation> e1 = new ArrayList<>();
        e1.add(e);

        Evaluation e2 = new Evaluation(null,null,"res");
        List<Evaluation> eval2 = new ArrayList<>();
        eval2.add(e2);

        Paper p = new Paper(p1, "t1", "d1", e1);
        p.setEvaluations(eval2);
        assertEquals(p.getEvaluations(),eval2);
    }
}
