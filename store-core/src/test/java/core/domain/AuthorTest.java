package core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class AuthorTest {

    @Test
    public void getPosition() {
        Proposal p = new Proposal();
        Author a = new Author("abc", p);
        assertEquals(a.getPosition(), "abc");
    }

    @Test
    public void getProposal() {
        Proposal p = new Proposal();
        Author a = new Author("abc", p);
        assertEquals(a.getProposal(), p);
    }

    @Test
    public void setPosition() {
        Proposal p = new Proposal();
        Author a = new Author("abc", p);
        a.setPosition("cba");
        assertEquals(a.getPosition(), "cba");
    }

    @Test
    public void setProposal() {
        Proposal p = new Proposal();
        Author a = new Author("abc", p);
        Proposal p2 = new Proposal(null, "a", null, null, null, null);
        a.setProposal(p2);
        assertEquals(a.getProposal(), p2);
    }
}
