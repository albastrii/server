package web.controller;

import core.domain.*;
import core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ConferenceController {
    @Autowired
    private ConferenceService conferenceService;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private PCMemberService pcMemberService;

    @Autowired
    private PaperService paperService;

    @Autowired
    private ProposalService proposalService;

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/conferences", method = RequestMethod.GET)
    List<Conference> getAllConferences() {
        return conferenceService.getConferences();
    }

    @RequestMapping(value = "/papers", method = RequestMethod.GET)
    List<Paper> getAllPapers() {
        return paperService.getAllPapers();
    }

    @RequestMapping(value = "/conferences/{id}", method = RequestMethod.GET)
    ResponseEntity<Conference> getConferenceById(@PathVariable Integer id) {
        Optional<Conference> op = conferenceService.findOneConference(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/conferences", method = RequestMethod.POST)
    ResponseEntity<String> saveConference(@RequestBody Conference conference) {
        boolean result = conferenceService.addConference(conference);
        if (result)
            return ResponseEntity.ok("Conference saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Conference was not saved!");
    }

    //   /sections?conferenceId=5
    @RequestMapping(value = "/sections", method = RequestMethod.POST)
    ResponseEntity<String> saveSection(@RequestBody Section section, @RequestParam Integer conferenceId) {
        boolean result = conferenceService.addSectionToConference(conferenceId, section);
        if (result)
            return ResponseEntity.ok("Section saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Section was not saved!");
    }

    // /sections?conferenceId=5
//    @RequestMapping(value = "/sections", method = RequestMethod.GET)
//    List<Section> getSectionsByConferenceId(@RequestParam Integer conferenceId) {
//        return sectionService.getSectionsByConferenceId(conferenceId);
//    }

    @RequestMapping(value = "/sections/{id}", method = RequestMethod.GET)
    ResponseEntity<Section> getSectionById(@PathVariable Integer id) {
        Optional<Section> op = sectionService.findOneSection(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    @RequestMapping(value = "/sections", method = RequestMethod.POST)
//    ResponseEntity<String> saveSection(@RequestBody Section section) {
//        boolean result = sectionService.addSection(section);
//        if (result)
//            return ResponseEntity.ok("Section saved successfully!");
//        else
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Section was not saved!");
//    }

//    @RequestMapping(value = "/sectionChairs", method = RequestMethod.POST)
//    ResponseEntity<String> saveSectionChair(@RequestBody SectionChair sectionChair) {
//        boolean result = sectionService.addSectionChair(sectionChair);
//        if (result)
//            return ResponseEntity.ok("SectionChair saved successfully!");
//        else
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("SectionChair was not saved!");
//    }
//
//    @RequestMapping(value = "/sectionChairs/{id}", method = RequestMethod.GET)
//    ResponseEntity<SectionChair> getSectionChairById(@PathVariable Integer id) {
//        Optional<SectionChair> op = sectionService.findOneSectionChair(id);
//        if (op.isPresent())
//            return ResponseEntity.ok(op.get());
//        else
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
//    }
//
//    // /sectionChairs?sectionId=5
//    @RequestMapping(value = "/sectionChairs", method = RequestMethod.GET)
//    ResponseEntity<SectionChair> getSectionChairBySectionId(@RequestParam Integer sectionId) {
//        Optional<SectionChair> op = sectionService.getSectionChairBySectionId(sectionId);
//        if (op.isPresent())
//            return ResponseEntity.ok(op.get());
//        else
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
//    }

    @RequestMapping(value = "/authors/{id}", method = RequestMethod.GET)
    ResponseEntity<Author> getAuthorById(@PathVariable Integer id) {
        Optional<Author> op = participantService.getAuthor(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    // /authors?conferenceId=5
//    @RequestMapping(value = "/authors", method = RequestMethod.GET)
//    List<Author> getAuthorsByConferenceId(@RequestParam Integer conferenceId) {
//        return participantService.getAuthorsByConferenceId(conferenceId);
//    }

    @RequestMapping(value = "/authors", method = RequestMethod.POST)
    ResponseEntity<String> saveAuthor(@RequestBody Author author) {
        boolean result = participantService.addAuthor(author);
        if (result)
            return ResponseEntity.ok("Author saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Author was not saved!");
    }

    @RequestMapping(value = "/listeners/{id}", method = RequestMethod.GET)
    ResponseEntity<Listener> getListenerById(@PathVariable Integer id) {
        Optional<Listener> op = participantService.getListener(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    // /listeners?conferenceId=5
//    @RequestMapping(value = "/listeners", method = RequestMethod.GET)
//    List<Listener> getListenersByConferenceId(@RequestParam Integer conferenceId) {
//        return participantService.getListenersByConferenceId(conferenceId);
//    }

    @RequestMapping(value = "/listeners", method = RequestMethod.POST)
    ResponseEntity<String> saveListener(@RequestBody Listener listener) {
        System.out.println("hereeeeeelistener");
        boolean result = participantService.addListener(listener);
        if (result)
            return ResponseEntity.ok("Listener saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Listener was not saved!");
    }

    @RequestMapping(value = "/fees", method = RequestMethod.POST)
    ResponseEntity<String> saveFee(@RequestBody Fee fee) {
        boolean result = participantService.addConferenceFee(fee);
        if (result)
            return ResponseEntity.ok("Fee saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Fee was not saved!");
    }

//    // /pcmembers?conferenceId=5
//    @RequestMapping(value = "/pcmembers", method = RequestMethod.GET)
//    List<PCMember> getPCMembersByConferenceId(@RequestParam Integer conferenceId) {
//        return pcMemberService.getPCMembersByConferenceId(conferenceId);
//    }

    @RequestMapping(value = "/pcmembers/{id}", method = RequestMethod.GET)
    ResponseEntity<PCMember> getPCMemberById(@PathVariable Integer id) {
        Optional<PCMember> op = pcMemberService.findOnePCMember(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/pcmembers", method = RequestMethod.POST)
    ResponseEntity<String> savePCMember(@RequestBody PCMember pcMember) {
        boolean result = pcMemberService.addPCMember(pcMember);
        if (result)
            return ResponseEntity.ok("PCMember saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("PCMember was not saved!");
    }

    @RequestMapping(value = "/reviewers/{id}", method = RequestMethod.GET)
    ResponseEntity<Reviewer> getReviewerById(@PathVariable Integer id) {
        Optional<Reviewer> op = pcMemberService.getReviewer(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    // /reviewers?conferenceId=5
//    @RequestMapping(value = "/reviewers", method = RequestMethod.GET)
//    List<Reviewer> getReviewersByConferenceId(@RequestParam Integer conferenceId) {
//        return pcMemberService.getReviewersByConferenceId(conferenceId);
//    }

    @RequestMapping(value = "/reviewers", method = RequestMethod.POST)
    ResponseEntity<String> saveReviewer(@RequestBody Reviewer reviewer) {
        boolean result = pcMemberService.addReviewer(reviewer);
        if (result)
            return ResponseEntity.ok("Reviewer saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Reviewer was not saved!");
    }

    @RequestMapping(value = "/chairs/{id}", method = RequestMethod.GET)
    ResponseEntity<Chair> getChairById(@PathVariable Integer id) {
        Optional<Chair> op = pcMemberService.getChair(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    // /chairs?conferenceId=5
//    @RequestMapping(value = "/chairs/conference", method = RequestMethod.GET)
//    List<Chair> getChairsByConferenceId(@RequestParam Integer conferenceId) {
//        return pcMemberService.getChairsByConferenceId(conferenceId);
//    }

    // /chairs?sectionId=5
    @RequestMapping(value = "/chairs/section", method = RequestMethod.GET)
    ResponseEntity<Chair> getChairBySectionId(@RequestParam Integer sectionId) {
        Optional<Chair> op = pcMemberService.getChairBySectionID(sectionId);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/chairs", method = RequestMethod.POST)
    ResponseEntity<String> saveChair(@RequestBody Chair chair) {
        boolean result = pcMemberService.addChair(chair);
        if (result)
            return ResponseEntity.ok("Chair saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Chair was not saved!");
    }

    @RequestMapping(value = "/papers/{id}", method = RequestMethod.GET)
    ResponseEntity<Paper> getPaperById(@PathVariable Integer id) {
        Optional<Paper> op = paperService.findOnePaper(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    // /papers?conferenceId=5
//    @RequestMapping(value = "/papers", method = RequestMethod.GET)
//    List<Paper> getPapersByConferenceId(@RequestParam Integer conferenceId) {
//        return paperService.getPapersByConferenceId(conferenceId);
//    }

    @RequestMapping(value = "/papers", method = RequestMethod.POST)
    ResponseEntity<String> savePaper(@RequestBody Paper paper) {
        boolean result = paperService.addPaper(paper);
        if (result)
            return ResponseEntity.ok("Paper saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Paper was not saved!");
    }

    @RequestMapping(value = "/evaluations", method = RequestMethod.POST)
    ResponseEntity<String> saveEvaluation(@RequestBody Evaluation evaluation) {
        boolean result = paperService.addEvaluation(evaluation);
        if (result)
            return ResponseEntity.ok("Evaluation saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Evaluation was not saved!");
    }

    @RequestMapping(value = "/proposals/{id}", method = RequestMethod.GET)
    ResponseEntity<Proposal> getProposalById(@PathVariable Integer id) {
        Optional<Proposal> op = proposalService.findOneProposal(id);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

//    // /proposals?conferenceId=5
//    @RequestMapping(value = "/proposals/conference", method = RequestMethod.GET)
//    List<Proposal> getProposalsByConferenceId(@RequestParam Integer conferenceId) {
//        return proposalService.getProposalsByConferenceId(conferenceId);
//    }

    // /proposals?authorId=5
    @RequestMapping(value = "/proposals", method = RequestMethod.GET)
    ResponseEntity<Proposal> getProposalByAuthorId(@RequestParam Integer authorId) {
        Optional<Proposal> op = proposalService.findProposalByAuthorId(authorId);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/proposals", method = RequestMethod.POST)
    ResponseEntity<String> saveProposal(@RequestBody Proposal proposal) {
        boolean result = proposalService.addProposal(proposal);
        if (result)
            return ResponseEntity.ok("Proposal saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Proposal was not saved!");
    }

//    @RequestMapping(value = "/biddings", method = RequestMethod.POST)
//    ResponseEntity<String> saveBidding(@RequestBody Bidding bidding) {
//        boolean result = proposalService.addBidding(bidding);
//        if (result)
//            return ResponseEntity.ok("Bidding saved successfully!");
//        else
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bidding was not saved!");
//    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    ResponseEntity<List<ExtendedLogin>> login(@RequestParam String username, @RequestParam String password) {
        List<ExtendedLogin> participants = loginService.login(username, password);
        if (participants.size() > 0)
            return ResponseEntity.ok(participants);
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    ResponseEntity<String> signUp(@RequestBody Login login) {
        boolean result = loginService.signUp(login);
        if (result)
            return ResponseEntity.ok("Signup successful!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Signup failed!");
    }

//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    ResponseEntity<String> login(@RequestBody Login login) {
//        boolean result = loginService.login(login);
//        if (result)
//            return ResponseEntity.ok("Login successful!");
//        else
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Login failed!");
//    }

    @RequestMapping(value = "/participants/{username}", method = RequestMethod.GET)
    ResponseEntity<Participant> getParticipantByUsername(@PathVariable String username) {
        Optional<Participant> op = participantService.getParticipantByUsername(username);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/pcmembers1/{username}", method = RequestMethod.GET)
    ResponseEntity<PCMember> getPCMembersByUsername(@PathVariable String username) {
        Optional<PCMember> op = pcMemberService.getPCMemberByUsername(username);
        if (op.isPresent())
            return ResponseEntity.ok(op.get());
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @RequestMapping(value = "/pcmembers", method = RequestMethod.GET)
    List<PCMember> getAllPCMembers() {
        return pcMemberService.getAllPCMembers();
    }

    @RequestMapping(value = "/reviewers", method = RequestMethod.GET)
    List<Reviewer> getAllReviewers() {
        return pcMemberService.getAllReviewers();
    }

    //   /sections1?listenerId=5
    @RequestMapping(value = "/sections1", method = RequestMethod.POST)
    ResponseEntity<String> saveSectionToListener(@RequestBody Section section, @RequestParam Integer listenerId) {
        boolean result = pcMemberService.addSectionToListener(listenerId, section);
        if (result)
            return ResponseEntity.ok("Section saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Section was not saved!");
    }

    //   /sections?sectionId=5
    @RequestMapping(value = "/sections", method = RequestMethod.GET)
    Long numberListenersPerSection(@RequestParam Integer sectionId) {
        return pcMemberService.numberListenersPerSection(sectionId);
    }

    @RequestMapping(value = "/conferences", method = RequestMethod.PUT)
    ResponseEntity<String> updateConference(@RequestBody Conference conference) {
        boolean result = conferenceService.updateConferenceDeadlines(conference);
        if (result)
            return ResponseEntity.ok("Conference saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Conference was not saved!");
    }

    //   /proposals1?conferenceId=5
    @RequestMapping(value = "/proposals1", method = RequestMethod.GET)
    List<Proposal> getProposalsByConference(@RequestParam Integer conferenceId) {
        return conferenceService.findProposalsByConference(conferenceId);
    }

    @RequestMapping(value = "/biddings", method = RequestMethod.POST)
    ResponseEntity<String> saveBidding(@RequestBody Bidding bidding) {
        boolean result = proposalService.addBidding(bidding);
        if (result)
            return ResponseEntity.ok("Bidding saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bidding was not saved!");
    }

    @RequestMapping(value = "/evaluations", method = RequestMethod.PUT)
    ResponseEntity<String> updateEvaluation(@RequestBody Evaluation evaluation) {
        boolean result = paperService.updateEvaluationResult(evaluation);
        if (result)
            return ResponseEntity.ok("Evaluation saved successfully!");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Evaluation was not saved!");
    }

    //   /papers1?reviewerId=5
    @RequestMapping(value = "/papers1", method = RequestMethod.GET)
    List<Paper> getPapersByReviewer(@RequestParam Integer reviewerId) {
        return paperService.getPapersByReviewer(reviewerId);
    }
}
